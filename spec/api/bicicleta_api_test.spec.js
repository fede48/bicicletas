var Bicicleta = require('../../models/bicicleta')
var server = require('../../bin/www')
var request = require('request')
var mongoose = require('mongoose')

var base_url = "http://localhost:3000/api/bicicletas";

describe("Bicicleta API", () => {
    beforeAll( (done) => {
        mongoose.connection.close().then(() => {
    
        var mongoDB = 'mongodb://localhost/red_bicicletas';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
        mongoose.set('useCreateIndex', true);
        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error: '));
        db.once('open', function () {
            console.log('We are connected to test database!');
            done();
        });
      });
    });

    afterEach((done) => {
        Bicicleta.deleteMany({}, function(error, success){
            if (error) console.log(error);
            done();
        });
    });

    describe('GET Bicicletas /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function(error,response,body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicleta.length).toBe(0);
                done();
            });
        });
    });

    describe('POST Bicicletas /create', () => {
        it('Status 200', (done) => {
          
          var abici = { "code":10,"color":"rojo","modelo":"Urbana","lat": 10.975832,"lng": -74.808815 };
          const options = 
          {
            url : 'http://localhost:3000/api/bicicletas/create',
            json: true,
            body: abici 
          };

          request.post(options,
              function(error, response, body){
                expect(response.statusCode).toBe(200);
                console.log(body);
                var bici = JSON.parse(JSON.stringify(body)).bicicleta;
                console.log(bici);
                expect(bici.color).toBe('rojo');
                expect(bici.ubicacion[0]).toBe(10.975832);
                expect(bici.ubicacion[1]).toBe(-74.808815);
                done();
              });
        });
    });
});

/*
beforeEach(() => {
    Bicicleta.allBicis = [];
})

describe('Bicicleta API', ()=> {
    
    describe('GET Bicicletas /', () =>{
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);
            var a = new Bicicleta(1,"rojo", "urbana",[-34.6012424,-58.3861497]);
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', function(error,response,body){
                expect(response.statusCode).toBe(200);
            });
        })
    })

    describe('POST Bicicletas /create', () => {
        it('Status 200', (done) => {
          
          var abici = { "id":2,"color":"Green","modelo":"Urbana","lat": 10.975832,"lng": -74.808815 };
          const options = 
          {
            url : 'http://localhost:3000/api/bicicletas/create',
            json: true,
            body: abici 
          }

          request.post(options,
              function(error, response, body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(2).color).toBe('Green');
                done();
              });
        });
    }) 
        
    describe('POST Bicicletas /update', () => {
        it('Status 200', (done) => {
            
            var a = new Bicicleta(3,"naranja", "carrera",[-34.6012436,-58.3861417]);
            Bicicleta.add(a);
            
            var abici = { "id": 3, "color":"Green","modelo":"Urbana","lat": 10.975832,"lng": -74.808815 };
            
            const options = 
            {
                url : 'http://localhost:3000/api/bicicletas/3/update',
                json: true,
                body: abici 
            }
            
            var ubicacion = [10.975832, -74.808815];
            request.post(options,
                function(error, response, body){
                //console.log(response.statusCode);
                //console.log(response.body);
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(3).color).toBe('Green');
                expect(Bicicleta.findById(3).modelo).toBe('Urbana');
                expect(Bicicleta.findById(3).ubicacion).toEqual(ubicacion);
                done();
                });
        });
    
    });

    describe('DELETE Bicicletas /delete', () => {
        it('Status 200', (done) => {
            
            var a = new Bicicleta(4,"naranja", "carrera",[-34.6012436,-58.3861417]);
            Bicicleta.add(a);
            
            var abici = { "id": 4 };
            
            const options = 
            {
                url : 'http://localhost:3000/api/bicicletas/delete',
                json: true,
                body: abici 
            }
            
            request.delete(options,
                function(error, response, body){
                expect(response.statusCode).toBe(204);
                expect(Bicicleta.allBicis.length).toBe(0);
                done();
                });
        });
    
    });
});
*/